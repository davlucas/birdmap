#!/usr/bin/env python
import geocoder
from geopy.geocoders import Nominatim
from geopy.distance import great_circle
import tkinter
from tkinter import Tk,Label,Entry
import webbrowser
import os
lang=os.getenv('LANG').split("_")[0]
loc=dict()
loc_addr=dict()
mypos=geocoder.ip('me')
mypos="{}, {}".format(mypos.latlng[0],mypos.latlng[1])
geolocator = Nominatim(user_agent="python")
place=geolocator.reverse(mypos,language=lang)
loc=place.raw
try:
    loc_addr=loc['address']
except:
    pass
###{'suburb': 'Seta', 'city': 'Otsu', 'state': 'Kinki Region', 'postcode': '520-2144', 'country': 'Japan', 'country_code': 'jp'}
#'school': 'École primaire Saint-Exupéry', 'road': "Quai de l'Aisne", 'neighbourhood': 'Cité des Auteurs', 'town': 'Pantin', 'county': 'Bobigny', 'state': 'Île-de-France', 'country': 'France', 'postcode': '93500', 'country_code': 'fr'}
try:
    city=loc_addr['city']
except:
    try:
        city=loc_addr['town']
    except:
        try:
            city=loc_addr['village']
        except:
            try:
                city=loc_addr['county']
            except:
                city=0
try:
    prov=loc_addr['state']
except:
    prov=1
try:
    country=loc_addr['country']
except:
    country=0
if prov==country:
    prov=loc_addr['county']





if isinstance(prov,int) and isinstance(country,int) and isinstance(city,int):
    width=21
else:
    width=max(len("city of {}".format(city)),len("Province of {}".format(prov)),len("in {}".format(country)),len("I'm in"))


def wikicity(event):
    webbrowser.open_new_tab(r"http://{}.wikipedia.org/wiki/{}".format(lang,city))
def wikiprov(event):
    webbrowser.open_new_tab(r"http://{}.wikipedia.org/wiki/{}".format(lang,prov))
def wikicountry(event):
    webbrowser.open_new_tab(r"http://{}.wikipedia.org/wiki/{}".format(lang,country))
def wikitravel(event):
    webbrowser.open_new_tab(r"https://wikitravel.org/wiki/en/index.php?search={}".format(city))


root=Tk()
root.title("Where am i")
Place=Label(root,width=width,text=f"I'm  in",bg='#FFFF90',fg='blue')
Place.grid(column=0,row=0,columnspan=1)
if isinstance(city,str) :
    city=city.split()[0]
    Infowiki=Label(root,width=width,text=f"City of {city}",bg='#FFFF00',fg='blue')
    Infowiki.grid(column=0,row=1)
    Infowiki.bind("<Button-1>", wikicity)
if isinstance(prov,str):
    prov=prov.split()[0]
    Infoprovwiki=Label(root,width=width,text=f"Province of {prov}",bg='#bbbb00',fg='blue')
    Infoprovwiki.grid(column=0,row=2)
    Infoprovwiki.bind("<Button-1>", wikiprov)
if isinstance(country,str):
    Infocountrywiki=Label(root,width=width,text=f" in {country}",bg='#999900',fg='blue')
    Infocountrywiki.grid(column=0,row=3)
    Infocountrywiki.bind("<Button-1>", wikicountry)
if isinstance(prov,int) and isinstance(country,int) and isinstance(city,int):
    NoLabel=Label(root,width=width,text="the middle of nowhere",bg='#FFFF90',fg='blue')
    NoLabel.grid(column=0,row=1)

root.mainloop()
