#!/usr/bin/python
"""
Distance with plane between 2 points
"""
from tkinter import (
    Listbox,
    Label,
    Tk,
    Button,
    Entry,
    LabelFrame,
    CENTER,
    messagebox,
    StringVar,
    ACTIVE,
)
from datetime import timedelta
import sys
from geopy.geocoders import Nominatim
from geopy.distance import great_circle


class App:
    def __init__(self):
        root = Tk()
        f_g = "maroon"
        b_g = "bisque"
        root.configure(background=b_g)
        self.speedbird = dict()
        self.speedbird["Duck"] = 85
        self.speedbird["Pigeon"] = 70
        self.speedbird["Goose"] = 140
        self.speedbird["Stork"] = 50
        self.speedbird["Swallow"] = 60
        self.speedbird["Rafale plane"] = 1320
        self.speedbird["Helicopter"] = 200
        self.speedbird["Dove"] = self.speedbird["Pigeon"]
        self.speedbird["Buzzard"] = 50
        oiseaux = [x for x, value in self.speedbird.items()]
        oiseaux.sort()
        self.depart = StringVar()
        self.answer = StringVar()
        self.arrivee = StringVar()
        frame = LabelFrame(
            root,
            bg=b_g,
            fg=f_g,
            font=("Helvetica", 12, "underline"),
            text="Departure",
        )
        Entry(frame, bg=b_g, fg=f_g, textvariable=self.depart).pack()
        frame.grid(column=0, row=0)
        frame = LabelFrame(
            root,
            bg=b_g,
            fg=f_g,
            font=("Helvetica", 12, "underline"),
            text="Arrival",
        )
        Entry(frame, bg=b_g, fg=f_g, textvariable=self.arrivee).pack()
        frame.grid(column=1, row=0)
        framebird = LabelFrame(
            root,
            bg=b_g,
            fg=f_g,
            font=("Helvetica", 12, "underline"),
            text="Bird",
        )
        self.listbox = Listbox(framebird, bg=b_g, fg=f_g, justify=CENTER)
        self.listbox.pack()
        framebird.grid(column=0, row=1, columnspan=2)
        for i in oiseaux:
            self.listbox.insert("end", i)
        Button(
            root, text="let's go", bg=f_g, fg=b_g, command=self.calcul_distance
        ).grid(column=0, row=2, columnspan=2)
        frameanswer=LabelFrame(root,width=50,height=20,bg='orange',fg='black',relief='ridge',borderwidth=4)
        Label(
            frameanswer,
            bg='orange',
            fg="black",
            width=50,
            height=5,
            font=("Noto", 12, "bold"),
            textvariable=self.answer,
        ).grid(column=0, row=3, columnspan=2)
        frameanswer.grid(column=0,row=3,columnspan=2)
        root.mainloop()

    def calcul_distance(self):
        depart = self.depart.get()
        arrivee = self.arrivee.get()
        bird = self.listbox.get(ACTIVE)
        geolocator = Nominatim(user_agent="webbirdmap")
        location_depart = geolocator.geocode(depart)
        location_arrivee = geolocator.geocode(arrivee)
        try:
            depart_gps = (location_depart.latitude, location_depart.longitude)
        except AttributeError:
            messagebox.showinfo("ERROR", "Not existing departure")
        try:
            arrivee_gps = (
                location_arrivee.latitude,
                location_arrivee.longitude,
            )
        except AttributeError:
            messagebox.showinfo("ERROR", "Not existing arrival")
        distancecal = str(great_circle(depart_gps, arrivee_gps).km).split(" ")
        dist = int(float(distancecal[0]))
        duree = str(
            timedelta(
                seconds=float((dist * 1000) / (self.speedbird[bird] / 3.6))
            )
        ).split(".", 2)[0]
        self.answer.set(
            "From {}  To {}\n  a distance of {} km\n flying like a {}\n you need {} \n".format(
                depart.upper(), arrivee.upper(), dist, bird, duree
            )
        )



def main(*args):
    """
    main
    """
    print("Launching the App")
    App()
    return args


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
